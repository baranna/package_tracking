import {PacketState} from './packet-state';
import {Product} from './product';

export class Packet {
  constructor(
    public packetId: string,
    public state: PacketState,
    public productList: Product[]
  ) {
  }
}
