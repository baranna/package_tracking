export enum PacketState {
  WfPU,
  PU,
  IP,
  OD,
  DD,
}

export class PacketStateEnumHelper {
  static getEnumLabel(state: PacketState) {
    switch (state) {
      case PacketState.WfPU:
        return 'Waiting for pickup';
      case PacketState.PU:
        return 'Picked up';
      case PacketState.IP:
        return 'In Depot';
      case PacketState.OD:
        return 'On Delivery';
      case PacketState.DD:
        return 'Delivered';
    }
  }
}
