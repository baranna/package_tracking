import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {PacketService} from '../packet.service';
import {Packet} from '../dto/packet';
import {MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger,} from '@angular/animations';
import {PacketState, PacketStateEnumHelper} from '../dto/packet-state';

@Component({
  selector: 'app-packet-information',
  templateUrl: './packet-information.component.html',
  styleUrls: ['./packet-information.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class PacketInformationComponent implements OnInit {
  packetId = new FormControl('', [
    Validators.required,
    Validators.pattern(/[A-Z]{4}\d{4}/),
  ]);
  displayedColumns: string[] = ['id', 'state'];
  packets: MatTableDataSource<Packet> = new MatTableDataSource([]);
  expandedElement: Packet | null;
  PacketStateEnumHelper = PacketStateEnumHelper;
  filtered = false;

  constructor(private packetService: PacketService) {
  }

  ngOnInit() {
    this.getPackets();
  }

  getPacket() {
    this.filtered = true;
    this.packetService.getPacket(this.packetId.value).subscribe(
      (res) => (this.packets = new MatTableDataSource<Packet>([res]))
    );
  }

  getPackets() {
    this.filtered = false;
    this.packetService
      .getPackets()
      .subscribe((res) => (this.packets = new MatTableDataSource<Packet>(res)));
  }

  getIconForDeliveryState(packetState: PacketState) {
    switch (packetState) {
      case PacketState.WfPU:
        return 'watch_later';
      case PacketState.PU:
        return 'backpack';
      case PacketState.IP:
        return 'widgets';
      case PacketState.OD:
        return 'local_shipping';
      case PacketState.DD:
        return 'home';
    }
  }
}
