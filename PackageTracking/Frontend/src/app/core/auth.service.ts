import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  get loggedIn(): boolean {
    return this.cookieService.check('PackageTrackingCookie');
  }

  constructor(private http: HttpClient,
              private cookieService: CookieService) {
  }

  login(email: string, password: string) {
    return this.http.post('http://localhost:4200/api/users/login', {email, password}, {withCredentials: true});
  }

  logout() {
    return this.http.get('http://localhost:4200/api/users/logout', {withCredentials: true});
  }
}
