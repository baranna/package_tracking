import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  get showLogout(): boolean {
    return this.authService.loggedIn;
  }
  constructor(private  authService: AuthService,
              private  router: Router) { }

  ngOnInit(): void {
  }

  logout() {
    this.authService.logout().subscribe((res) => this.router.navigateByUrl('/login'));
  }

}
