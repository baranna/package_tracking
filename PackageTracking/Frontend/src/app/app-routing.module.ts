import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PacketInformationComponent } from './packet-information/packet-information.component';
import {AuthGuard} from './core/auth.guard';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: PacketInformationComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
