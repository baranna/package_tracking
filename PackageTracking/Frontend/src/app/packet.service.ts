import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Packet } from "./dto/packet";
import { PacketState } from "./dto/packet-state";
import { Product } from "./dto/product";
import { TitleCasePipe } from "@angular/common";

@Injectable({
  providedIn: "root",
})
export class PacketService {
  constructor(private http: HttpClient) {}

  getPacket(id: string): Observable<Packet> {
    return this.http.get<Packet>(`http://localhost:4200/api/packets/${id}`, {
      withCredentials: true,
    });
  }

  getPackets(): Observable<Packet[]> {
    return this.http.get<Packet[]>("http://localhost:4200/api/packets", {
      withCredentials: true,
    });
  }
}
