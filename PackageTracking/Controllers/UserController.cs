﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PackageTracking.Common;
using PackageTracking.DAL.Seed;
using PackageTracking.Models;

namespace PackageTracking.Controllers
{
    [AllowAnonymous]
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private readonly SignInManager<PackageApplicationUser> signInManager;
        private readonly UserManager<PackageApplicationUser> userManager;
        private readonly Seeder seeder;

        public UserController(SignInManager<PackageApplicationUser> signInManager, UserManager<PackageApplicationUser> userManager, Seeder seeder)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.seeder = seeder;
        }

        [HttpPost("login")]
        public async Task<ActionResult> LoginAsync([FromBody] LoginModel loginModel)
        {
            var user = await userManager.FindByEmailAsync(loginModel.Email);
            if (user != null)
            {
                var result = await signInManager.PasswordSignInAsync(user, loginModel.Password, true, false);
                if (result.Succeeded)
                    return Ok();
            }

            return Unauthorized();
        }

        [HttpGet("logout")]
        public async Task<ActionResult> LogoutAsync()
        {
            await signInManager.SignOutAsync();
            return Ok();
        }

        [HttpGet("seed")]
        public async Task<ActionResult> SeedDatabaseAsync()
        {
            await seeder.SeedAsync();
            return Ok();
        }
    }
}