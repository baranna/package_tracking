﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PackageTracking.BLL.Services.Interfaces;
using PackageTracking.Common;
using PackageTracking.Common.Entities;
using PackageTracking.Models;

namespace PackageTracking.Controllers
{
    [Authorize]
    [Route("api/packets")]
    [ApiController]
    public class PacketController : ControllerBase
    {
        private readonly UserManager<PackageApplicationUser> userManager;
        private readonly IPackageService packageService;

        public PacketController(UserManager<PackageApplicationUser> userManager, IPackageService packageService)
        {
            this.userManager = userManager;
            this.packageService = packageService;
        }

        [HttpGet]
        public async Task<ActionResult<List<Packet>>> GetPacketsForUserAsync()
        {
            var user = await userManager.GetUserAsync(User);
            var packets = await packageService.GetPacketsForUserAsync(user.Id);

            return Ok(packets);

        }

        [HttpGet("{packetId}")]
        public async Task<ActionResult<Packet>> GetPacketAsync([FromRoute] string packetId)
        {
            var packet = await packageService.GetPackageAsync(packetId);
            if (packet == null)
            {
                return NotFound();
            }
            PackageApplicationUser user = await userManager.GetUserAsync(User);
            if (user.Packets.Contains(packet))
                return Ok(packet);
            return Unauthorized();
        }
        
    }
}