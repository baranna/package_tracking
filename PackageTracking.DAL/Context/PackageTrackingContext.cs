﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PackageTracking.Common;
using PackageTracking.Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PackageTracking.DAL.Context
{
    public class PackageTrackingContext : IdentityDbContext<PackageApplicationUser, IdentityRole, string>
    {
        public PackageTrackingContext(DbContextOptions<PackageTrackingContext> options) : base(options)
        {
            Database.EnsureCreated();

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

        }

        public DbSet<Packet> Packets { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
