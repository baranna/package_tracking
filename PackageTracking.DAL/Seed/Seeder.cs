﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using PackageTracking.Common;
using PackageTracking.Common.Entities;
using PackageTracking.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Html.Parser;
using AngleSharp.Dom;
using System.Text.RegularExpressions;

namespace PackageTracking.DAL.Seed
{
    public class Seeder
    {
        private readonly PackageTrackingContext context;
        private readonly UserManager<PackageApplicationUser> userManager;

        public Seeder(PackageTrackingContext context, UserManager<PackageApplicationUser> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        public async Task SeedAsync()
        {
            try
            {
                var result = context.Users.Where(u => u.UserName == "Admin");
                if (result.Any())
                {
                    return;
                }

                var products = await GetProducts();

                var packets = new List<Packet>
            {
                new Packet { PacketId = "SAMS1234", State = PacketState.OD, ProductList = products.Take(10).ToList() },
                new Packet { PacketId = "WSDQ1234", State = PacketState.IP,  ProductList = products.Skip(10).Take(5).ToList()},
                new Packet { PacketId = "WRTW1234", State = PacketState.PU,  ProductList = products.Skip(15).Take(4).ToList()},
                new Packet { PacketId = "OPFM1234", State = PacketState.WfPU,  ProductList = products.Skip(19).Take(1).ToList()},
                new Packet { PacketId = "DMGR1234", State = PacketState.DD,  ProductList = products.Skip(20).Take(2).ToList()},
            };

                context.Roles.Add(new IdentityRole { Name = "ADMIN", NormalizedName = "ADMIN" });

                var user = new PackageApplicationUser
                {
                    Email = "admin@admin.com",
                    NormalizedEmail = "ADMIN@ADMIN.COM",
                    UserName = "Admin",
                    NormalizedUserName = "ADMIN",
                    EmailConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    LockoutEnabled = false,
                    PhoneNumberConfirmed = true,
                    PhoneNumber = "+111111111111",
                    AccessFailedCount = 0,
                    ConcurrencyStamp = "secret",
                    TwoFactorEnabled = false,
                    Packets = packets
                };

                var password = new PasswordHasher<PackageApplicationUser>();
                var hashed = password.HashPassword(user, "P@ssword4!");
                user.PasswordHash = hashed;

                await userManager.CreateAsync(user);
                await userManager.AddToRoleAsync(user, "ADMIN");
                await context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private async Task<ICollection<Product>> GetProducts()
        {
            var result = new List<Product>();
            var config = Configuration.Default.WithDefaultLoader();
            var context = BrowsingContext.New(config);
            var url = "https://www.arukereso.hu/mobiltelefon-c3277/f:samsung,erintokepernyo/";
        
            while (url != null)
            {
                var document = await context.OpenAsync(url);
                var page = GetPageData(document, url);
                result.AddRange(page);
                var paginationContainer = document.QuerySelector(".pagination");
                url =  paginationContainer.Children[paginationContainer.ChildElementCount-2].GetAttribute("Href");
            }           

            return result;
        }

        private ICollection<Product> GetPageData(IDocument document, string url)
        {
            var result = new List<Product>();
            var productRows = document.QuerySelector(".list-view").Children;

            foreach (var productRow in productRows)
            {
                var nameContainer = productRow.QuerySelector(".name");
                var name = nameContainer?.FirstElementChild?.FirstElementChild?.TextContent;
                var priceWithCurrency = productRow.QuerySelector(".price")?.TextContent;
                if (priceWithCurrency != null && name != null)
                {
                    var price = Int32.Parse(Regex.Match(priceWithCurrency, @"[\d\s]+").Value.Replace(" ", ""));
                    result.Add(new Product { Name = name, Price = price });
                }                
                
            }
            return result;

        }
}
}
