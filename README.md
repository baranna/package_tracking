# Package Tracking Application
Demo app for tracking delivery states

## Backend
The database is created at application start (code-first).
Seeding the database is through the users/seed endpoint, it inserts an admin user with packages of products.

## Frontend
Run frontend by npm start to set proxy-config.
