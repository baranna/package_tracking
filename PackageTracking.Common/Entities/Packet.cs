﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PackageTracking.Common.Entities
{
    public class Packet
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string PacketId { get; set; }

        public PacketState State { get; set; }

        public ICollection<Product> ProductList { get; set; }
    }
}
