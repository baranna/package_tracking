﻿using Microsoft.AspNetCore.Identity;
using PackageTracking.Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PackageTracking.Common
{
    public class PackageApplicationUser : IdentityUser
    {
        public ICollection<Packet> Packets { get; set; }
    }
}
