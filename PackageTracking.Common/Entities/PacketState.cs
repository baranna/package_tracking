﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageTracking.Common.Entities
{
    public enum PacketState
    {
        WfPU,
        PU,
        IP,
        OD,
        DD,
    }
}
