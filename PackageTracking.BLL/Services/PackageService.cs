﻿using Microsoft.EntityFrameworkCore;
using PackageTracking.BLL.Services.Interfaces;
using PackageTracking.Common.Entities;
using PackageTracking.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace PackageTracking.BLL.Services
{
    public class PackageService : IPackageService
    {
        private readonly PackageTrackingContext db;

        public PackageService(PackageTrackingContext db)
        {
            this.db = db;
        }

        public async Task<Packet> GetPackageAsync(string id)
        {
            return await db.Packets.Include(packet => packet.ProductList).FirstOrDefaultAsync(packet => packet.PacketId == id);
        }

        public async Task<ICollection<Packet>> GetPacketsForUserAsync(string userId)
        {
            var user = await db.Users.Include(u => u.Packets).ThenInclude(p => p.ProductList).FirstOrDefaultAsync(u => u.Id == userId);
            return user?.Packets;           
        }
    }
}
