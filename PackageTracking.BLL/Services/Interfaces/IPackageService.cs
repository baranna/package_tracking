﻿using PackageTracking.Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PackageTracking.BLL.Services.Interfaces
{
    public interface IPackageService
    {
        Task<Packet> GetPackageAsync(string id);

        Task<ICollection<Packet>> GetPacketsForUserAsync(string userId);
    }
}
